// @ts-nocheck
import axios from "axios";
import * as ROUTES from "../routes/routes";

axios.defaults.headers.common = {
  "Content-Type": "application/json",
  "X-Requested-With": "XMLHttpRequest"
};
class Api {
  static getPlaces(state) {
    axios({
      method: "get",
      url: ROUTES.ROOT + ROUTES.PLACES
    })
      .then(function(response) {
        let data = response.data.places;
        state({ places: data });
      })
      .catch(function(response) {
        console.log(response);
      });
  }

  static login(values) {
    axios({
      method: "post",
      url: "/auth/login",
      data: values
    })
      .then(function(response) {
        let token = response.data.access_token;
        console.log(token);
      })
      .catch(function(response) {
        let message = JSON.parse(response.request.response);
        console.log(message);
      });
  }

  static getEvents(state) {
    axios({
      method: "get",
      url: ROUTES.ROOT + ROUTES.EVENTS
    })
      .then(function(response) {
        let data = response.data.events;
        // console.log("RESPOINSE ", data);
        state({ events: data });
      })
      .catch(function(response) {
        console.log(response);
      });
  }
}

export default Api;
