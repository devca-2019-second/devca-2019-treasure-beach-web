// @ts-check
/* eslint-disable */
class Misc {
  static greet() {
    var myDate = new Date();
    var hrs = myDate.getHours();
    var greet;

    if (hrs < 12) greet = "Good Morning";
    else if (hrs >= 12 && hrs <= 17) greet = "Good Afternoon";
    else if (hrs >= 17 && hrs <= 19) greet = "Good Evening";
    else if (hrs >= 19 && hrs <= 24) greet = "Good Night";
    return greet;
  }

  static clickOutsideEvent(evt, id) {
    const modal = document.getElementById(id);
    var targetElement = evt.target;
    do {
      if (targetElement == modal) {
        return;
      }
      targetElement = targetElement.parentNode;
    } while (targetElement);
    window.history.back();
  }

  static escKeyGoBack(event) {
    if (event.keyCode === 27) {
      window.history.back();
    }
  }

  static modalEnterUtils(clickOut, escKey) {
    document.body.classList.add("noscroll");
    document.addEventListener("keydown", escKey, false);
    document.addEventListener("click", clickOut, false);
  }

  static modalExitUtils(clickOut, escKey) {
    document.body.classList.remove("noscroll");
    document.removeEventListener("keydown", escKey, false);
    document.removeEventListener("click", clickOut, false);
  }

  static unMaskCurrency(value) {
    if (typeof value != "number") value = parseFloat(value.replace(/,/g, ""));
    return value;
  }

  static toggleLargeFont() {
    localStorage.getItem("large font") === "true"
      ? localStorage.setItem("large font", "false")
      : localStorage.setItem("large font", "true");
    Misc.fontSizeApply();
  }

  static isFontLarge() {
    return localStorage.getItem("large font") === "true";
  }

  /**
   * Responsible for changing font-size
   *
   *
   */
  static fontSizeApply() {
    let root = document.documentElement;
    if (Misc.isFontLarge()) {
      root.style.setProperty("--text-base-size", "1.3em");
    } else {
      root.style.setProperty("--text-base-size", "1.1em");
    }
  }
}

export default Misc;
