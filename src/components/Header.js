import React from "react";
import { NavLink } from "react-router-dom";
import * as ROUTES from "../routes/routes";
import * as ENV from "../config/env";
import "../styles/header.css";

const Header = () => (
  <header>
    <nav>
      <ul className="header shadow">
        <li>
          <NavLink
            activeClassName="activelink"
            className="link"
            exact
            to={ROUTES.EVENTS}
          >
            Events
          </NavLink>
        </li>
        <li>
          <NavLink
            activeClassName="activelink"
            className="link"
            exact
            to={ROUTES.MAP}
          >
            Map
          </NavLink>
        </li>
        <li id="logo">
          <NavLink
            exact
            activeClassName="activelogo"
            id="logochild"
            to={ROUTES.HOME}
          >
            {ENV.APP_NAME}
          </NavLink>
        </li>
      </ul>
    </nav>
  </header>
);

export default Header;
