import React from "react";
import "./scroll.css";
import { Link } from "react-router-dom";

const HorizontalScroll = () => (
  <div className="scrollmenu">
    <Link
      to="#"
      id="myBtn"
      className="image-shadow"
      style={{
        backgroundImage: "url(https://source.unsplash.com/Koei_7yYtIo/400x230)"
      }}
    >
      <div className="info">
        <h2>Hotels</h2>
      </div>
    </Link>

    <Link
      to="#"
      className="image-shadow"
      style={{
        backgroundImage: "url(https://source.unsplash.com/OFismyezPnY/400x230)"
      }}
    >
      <div className="info">
        <h2>Cuisine</h2>
      </div>
    </Link>

    <Link
      to="#"
      className="image-shadow"
      style={{
        backgroundImage: "url(https://source.unsplash.com/400x230/?food)"
      }}
    >
      <div className="info">
        <h2>Cuisine</h2>
      </div>
    </Link>

    <Link
      to="#"
      className="image-shadow"
      style={{
        backgroundImage: "url(https://source.unsplash.com/400x230/?food)"
      }}
    >
      <div className="info">
        <h2>Cuisine</h2>
      </div>
    </Link>
  </div>
);

export default HorizontalScroll;
