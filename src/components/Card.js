import React from "react";
import * as ROUTES from "../routes/routes";
import { Link } from "react-router-dom";

const Card = ({ places }) => (
  <div className="container">
    {places.map(i => (
      <Link
        className="s-card item"
        to={ROUTES.EVENTS}
        style={{ margin: "16px" }}
      >
        <img
          width="300"
          height="250"
          src="https://source.unsplash.com/bAK9wQghnHI/600x300"
        />
        <div className="card-container">
          <h4>{i.name}</h4>
          <h5 style={{ color: "var(--color)" }}>{i.region}</h5>
        </div>
      </Link>
    ))}
  </div>
);

export default Card;
