import React from "react";

let copy = [
  "Milking the coconuts",
  "Loading",
  "Combing the palms",
  "Preparing warm waters",
  "Training the seagulls",
  "Whitening the sand",
  "Cooling the breeze",
  "Widening our smiles",
  "Everything soon Irie",
  "Readying your stay",
  "Hold on mon!"
];
let i = Math.floor(Math.random() * 9 + 0);
let word = copy[i];
const Loading = () => <h2 style={{ textAlign: "center" }}>{word}...</h2>;
export default Loading;
