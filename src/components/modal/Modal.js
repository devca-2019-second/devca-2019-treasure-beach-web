import React from "react";
import Misc from "../../utils/misc";
// import "../../styles/animate.css";
import "./modal.css";

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.escFunction = this.escFunction.bind(this);
    this.outClick = this.outClick.bind(this);
  }

  outClick(evt) {
    Misc.clickOutsideEvent(evt, "modal-content-animation");
  }

  escFunction(event) {
    Misc.escKeyGoBack(event);
  }

  componentDidMount() {
    Misc.modalEnterUtils(this.outClick, this.escFunction);
  }
  componentWillUnmount() {
    Misc.modalExitUtils(this.outClick, this.escFunction);
  }

  render() {
    const back = e => {
      e.stopPropagation();
      window.history.back();
    };
    return (
      <div id="modal" className="modal">
        <div style={{ marginBottom: "70px" }} />
        <div id="modal-content-animation" className="modal-content fadeInUp">
          <button
            className="close"
            type="button"
            title="Close"
            id="modal-btn"
            onClick={back}
          >
            &#10005;
          </button>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Modal;
