import { Route } from "react-router-dom";
import React from "react";
import Modal from "./Modal";

const ModalRoute = ({ component: Comp, path, ...rest }) => {
  return (
    <Route
      path={path}
      {...rest}
      render={props => {
        return (
          <Modal>
            <Comp {...props} />
          </Modal>
        );
      }}
    />
  );
};

export default ModalRoute;
