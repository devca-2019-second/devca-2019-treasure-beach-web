import React from "react";
/**
 * Props : width, height, fill, y, stroke
 */
const IconHome = props => {
  return (
    <div
      className={props.className}
      style={{
        display: "inline-block",
        lineHeight: 0
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width={props.width === undefined ? "1em" : props.width}
        height={props.height === undefined ? "1em" : props.height}
        // viewBox="0 0 24 24"
        viewBox={`0 ${props.y === undefined ? 0 : props.y} 24 24`}
        stroke={props.fill === undefined || "" ? "currentColor" : props.fill}
        fill={props.colorFill === undefined || "" ? "none" : props.colorFill}
        strokeWidth={props.stroke === undefined || "" ? "2" : props.stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
      >
        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z" />
        <polyline points="9 22 9 12 15 12 15 22" />
      </svg>
    </div>
  );
};
export default IconHome;
