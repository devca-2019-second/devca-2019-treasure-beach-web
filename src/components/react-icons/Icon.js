import React from "react";
/**
 * Props : width, height, fill, y, stroke
 */
const Icon = props => {
    return (
        <div
            className={props.className}
            style={{
                display: "inline-block",
                lineHeight: 0
            }}
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width={props.width === undefined ? "1em" : props.width}
                height={props.height === undefined ? "1em" : props.height}
                // viewBox="0 0 24 24"
                viewBox={`0 ${props.y === undefined ? 0 : props.y} 24 24`}
                stroke={
                    props.fill === undefined || "" ? "currentColor" : props.fill
                }
                fill={
                    props.colorFill === undefined || ""
                        ? "none"
                        : props.colorFill
                }
                strokeWidth={
                    props.stroke === undefined || "" ? "2" : props.stroke
                }
                strokeLinecap="round"
                strokeLinejoin="round"
            >
                {props.children}
            </svg>
        </div>
    );
};
export default Icon;
