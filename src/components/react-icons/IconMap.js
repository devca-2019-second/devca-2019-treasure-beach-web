import React from "react";
/**
 * Props : width, height, fill, y, stroke
 */
const IconMap = props => {
  return (
    <div
      className={props.className}
      style={{
        display: "inline-block",
        lineHeight: 0
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width={props.width === undefined ? "1em" : props.width}
        height={props.height === undefined ? "1em" : props.height}
        // viewBox="0 0 24 24"
        viewBox={`0 ${props.y === undefined ? 0 : props.y} 24 24`}
        stroke={props.fill === undefined || "" ? "currentColor" : props.fill}
        fill={props.colorFill === undefined || "" ? "none" : props.colorFill}
        strokeWidth={props.stroke === undefined || "" ? "2" : props.stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
      >
        <polygon points="1 6 1 22 8 18 16 22 23 18 23 2 16 6 8 2 1 6" />
        <line x1="8" y1="2" x2="8" y2="18" />
        <line x1="16" y1="6" x2="16" y2="22" />
      </svg>
    </div>
  );
};
export default IconMap;
