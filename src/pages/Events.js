import React, { Component } from "react";
import Api from "../utils/apihelper";
import axios from "axios";
import * as ROUTES from "../routes/routes";
import Card from "../components/Card";

class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      events: []
    };
  }

  componentWillMount() {
    this.getEvents();
  }

  getEvents = async () => {
    const data = await axios.get(ROUTES.ROOT + ROUTES.EVENTS);

    this.setState({ events: data.data.events });
  };

  render() {
    return <div className="page">EVENTS</div>;
  }
}

componentName.propTypes = {};

export default componentName;
