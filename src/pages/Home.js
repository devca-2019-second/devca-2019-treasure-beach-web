import React, { Component } from "react";
import * as ENV from "../config/env";
import IconHome from "../components/react-icons/IconHome";
import IconMap from "../components/react-icons/IconMap";
import IconCalender from "../components/react-icons/IconCalender";
import HorizontalScroll from "../components/scroll/HorizontalScroll";
import { Link } from "react-router-dom";
import * as ROUTES from "../routes/routes";
import Card from "../components/Card";
import Loading from "../components/Loading";

class Home extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="home" style={{ paddingTop: "-40vh" }}>
        <div>
          <div
            style={{
              backgroundImage: `url(https://source.unsplash.com/${
                ENV.LANDING_IMG
              }/1600x900)`,
              backgroundRepeat: "none",
              paddingTop: "140px",
              paddingBottom: "90px",
              marginBottom: "0px",
              height: "100px",
              filter: "blur(8px)"
            }}
          />
          <h1
            className="grid-center"
            style={{
              transform: "translate(0px,-182px)",
              height: "0px",
              fontSize: "48px"
            }}
          >
            Explore Treasure Beach
          </h1>
        </div>

        <div className="grid-center card-flow">
          <div className="card item">
            <div className="card-container">
              <h3>
                <span style={{ color: "var(--secondary-color)" }}>
                  <IconHome y="-1" stroke="3" /> Stay with a local
                </span>
              </h3>
            </div>
          </div>

          <Link className="card item" to={ROUTES.EVENTS}>
            <div className="card-container">
              <h3>
                <span style={{ color: "var(--secondary-color)" }}>
                  <IconCalender y="-1" stroke="3" /> View Events
                </span>
              </h3>
            </div>
          </Link>

          <div className="card item">
            <div className="card-container">
              <h3>
                <span style={{ color: "var(--secondary-color)" }}>
                  <IconMap y="-1" stroke="3" /> View Map
                </span>
              </h3>
            </div>
          </div>
        </div>

        <br />

        <div className="page-container ">
          <HorizontalScroll />
          {this.props.places.length == 0 ? <Loading /> : null}
          <Card places={this.props.places} />
        </div>
        <br />
      </div>
    );
  }
}

export default Home;
