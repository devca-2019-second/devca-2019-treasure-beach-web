import React from "react";
import MapGl from "../components/MapGl";

function Map() {
  return (
    <div className="home" style={{ paddingTop: "40px" }}>
      <h1>Map</h1>
      {/* <BottomBar /> */}
      <MapGl />
    </div>
  );
}

export default Map;
