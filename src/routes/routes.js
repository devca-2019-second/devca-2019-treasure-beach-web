//PUBLIC
export const HOME = "/";
export const FEED = "/feed";
export const ID = "/:id";
export const MAP = "/map";
export const EVENTS = "/events";

//API
export const ROOT = "http://192.168.0.19:5000";
export const PLACES = "/places";
export const BOOKINGS = "/bookings";
