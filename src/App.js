import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import Feed from "./pages/Feed";
import Header from "./components/Header";
import axios from "axios";
import * as ROUTES from "./routes/routes";
import * as ENV from "./config/env";
import ModalRoute from "./components/modal/ModalRoute";
import Map from "./pages/Map";
import Api from "./utils/apihelper";
import "./styles/app.css";

import Events from "./pages/Events";

document.title = ENV.APP_NAME;
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      previousLocation: props.location,
      apiresponse: [],
      places: []
    };
  }

  componentDidMount() {
    document.title = ENV.APP_NAME;
    let url = "https://api.sheety.co/f1db1680-2a10-47cc-b0fd-cad4ff77e9f7";
    axios
      .get(url)
      .then(response => {
        this.setState({ apiresponse: response.data });
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      });

    Api.getPlaces(this.setState.bind(this));
  }

  previousLocation = this.props.location;
  componentWillUpdate(nextProps) {
    const { location } = this.props;
    // set previousLocation if props.location is not modal
    if (
      nextProps.history.action !== "POP" &&
      (!location.state || !location.state.modal)
    ) {
      this.previousLocation = this.props.location;
    }
  }

  render() {
    const { location } = this.props;
    const isModal = !!(
      location.state &&
      location.state.modal &&
      this.previousLocation !== location
    ); // not initial render
    return (
      <div>
        <Header />
        <div>
          <Switch location={isModal ? this.previousLocation : location}>
            <Route
              exact
              path={ROUTES.HOME}
              render={props => <Home {...props} places={this.state.places} />}
            />

            <Route exact path={ROUTES.MAP} component={Map} />

            <Route
              exact
              path={ROUTES.FEED}
              render={props => <Feed {...props} data={this.state.places} />}
            />

            <Route exact path={ROUTES.EVENTS} component={Events} />
          </Switch>
        </div>

        {/* {isModal ? (
          <Route
            exact
            path={ROUTES.FEED}
            render={props => (
              <Modal {...props}>
                <Feed {...props} data={this.state.apiresponse} />
              </Modal>
            )}
          />
        ) : null} */}
      </div>
    );
  }
}

export default App;
