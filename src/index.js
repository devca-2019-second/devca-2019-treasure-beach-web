import React from "react";
import { render } from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./styles/app.css";

import App from "./App";

const Hammock = () => (
  <Router>
    <Route component={App} />
  </Router>
);

render(<Hammock />, document.getElementById("root"));
